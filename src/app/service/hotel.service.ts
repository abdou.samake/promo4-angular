import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {combineLatest, Observable, pipe} from 'rxjs';
import {HotelModel} from '../model/hotel.model';
import {map, switchMap, tap} from 'rxjs/operators';
import {RoomModel} from '../model/room.model';


@Injectable({
  providedIn: 'root'
})
export class HotelService {
  root: string = environment.api;
  hotel: HotelModel;
  constructor(
    private httpClient: HttpClient
  ){ }

  getAllHotel(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hotels') as Observable<HotelModel[]>;
  }
  createNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.post(this.root + 'hotels', hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
        );
  }
  deleteHotelById$(hotelId: string): Observable<string> {
    return (this.httpClient.delete(this.root + 'hotels/' + hotelId) as Observable<string>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  putHotelById(hotelId: string, newHotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.put(this.root + 'hotels/' + hotelId, newHotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  patchHotelById$(hotelId: string, newHotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.patch(this.root + 'hotels/' + hotelId, newHotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  getHotelById$(hotelId: string): Observable<any> {
    return (this.httpClient.get(this.root + 'hotels/' + hotelId) as Observable<HotelModel>)
      .pipe(
        tap((hotel: HotelModel) => this.hotel = hotel),
        switchMap(hotel => combineLatest((hotel.rooms).map((roomId) => this.getRoomById$(roomId)))),
        tap((rooms: RoomModel[]) => this.hotel.roomData = rooms),
        map(() => this.hotel)
      );
  }
  getRoomById$(roomId: string): Observable<RoomModel> {
    return (this.httpClient.get(this.root + 'rooms/' + roomId) as Observable<RoomModel>);
  }
}




