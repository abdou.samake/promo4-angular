import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Menu1Component } from './menu1/menu1.component';
import { Menu2Component } from './menu2/menu2.component';
import { MenuLogoComponent } from './menu-logo/menu-logo.component';
import {RouterModule} from '@angular/router';




@NgModule({
  declarations: [
    Menu1Component,
    Menu2Component,
    MenuLogoComponent,
  ],
  exports: [
    Menu1Component,
    Menu2Component,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class MainMenuModule { }
