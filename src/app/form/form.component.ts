import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
myForm: FormGroup;
  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  this.myForm = this.fb.group({
    name: ['hotel des fleurs', [Validators.required, Validators.maxLength(5)]],
    roomNumbers: [null, []],
    pool: [false, [Validators.requiredTrue]]
  });
  }
  sendForm(): void {
    if (this.myForm.valid) {
      console.log(this.myForm.value);
    }else {
      alert('le formulaire est invalid');
    }
  }

}
