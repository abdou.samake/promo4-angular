import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './model/menu.model';
import {MenuService} from './service/menu.service';
import {MoodService} from './service/mood.service';
import {takeUntil, tap} from 'rxjs/operators';
import {combineLatest, Subject, Subscription} from 'rxjs';
import {HotelService} from './service/hotel.service';
import {HotelModel} from "./model/hotel.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'promo4Angular';
  destroy$: Subject<boolean> = new Subject();
  message: string;
  allRoutes: MenuModel[] = this.menuService.allRoutes;
  hotel: HotelModel;
  age = 50;
  constructor(
    private menuService: MenuService,
    private moodService: MoodService,
    private hotelService: HotelService
  ) {
  }
  ngOnInit(): void {
    combineLatest([this.hotelService.getAllHotel()
      .pipe(
        tap(x => console.log(x))
      ),
      this.moodService.handleMood$
        .pipe(
          tap((message: string) => this.message = message)
        )])
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
  createHotel(): void {
    this.hotelService.createNewHotel$({
      name: 'hotel des roses',
    roomNumbers: 20,
    pool: true
    })
      .subscribe();
  }
  deleteHotel(): void {
    this.hotelService.deleteHotelById$('Q9R3CzyBQOk2VyW6McVE')
      .subscribe();
  }
  putHotel(): void {
    this.hotelService.putHotelById('8jF9JEE9GUFe6FCFwPTw', {
      name: 'Novotel',
    roomNumbers: 30,
    pool: true
    })
      .subscribe();
  }
  patchHotel(): void {
    this.hotelService.patchHotelById$('AonmSa4BlVmUL0XHMHmt', {
      name: 'Rabbuson Blue'
    }).subscribe();
  }
  getHotel() {
    this.hotelService.getHotelById$('IWPMFMCLHsnLlBxqEvdF')
      .pipe(
        tap((hotel: HotelModel) => this.hotel = hotel)
      )
      .subscribe();
  }
getRoom() {
    this.hotelService.getRoomById$('TTchhMxW79EeUJitgVf3')
      .pipe(
        tap(x => console.log(x))
      )
      .subscribe();
}
}



