import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeModule } from './home/home.module';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  {
    path: 'à-propos',
    loadChildren: () => import('./about/about.module').then(m => m.AboutModule)
  },
  { path: 'form', loadChildren: () => import('./form/form.module').then(m => m.FormModule) }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes), HomeModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
