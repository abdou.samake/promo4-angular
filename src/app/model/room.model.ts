export interface RoomModel {
    roomName?: string;
    size?: number;
    id?: number | string;
     NumberDoor?: number;
    // doors: number[];
    roomState?: RoomState;
}

export enum RoomState {
    isavailable = 'est disponible',
    isoccuped = 'est occupé',
    isclosed = 'est en traveaux',
    iscleaning = 'en nettoyage'
}

